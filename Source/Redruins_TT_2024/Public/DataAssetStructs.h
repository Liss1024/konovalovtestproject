// RedRuins Softworks (c)

#pragma once

#include "CoreMinimal.h"
#include "DataAssetStructs.generated.h"

class UDataAsset2;

USTRUCT(BlueprintType)
struct FDataAsset1Struct
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UDataAsset2* DataAsset2;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool SomeCheckBox = false;
};
