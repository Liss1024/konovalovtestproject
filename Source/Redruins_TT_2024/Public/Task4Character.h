// RedRuins Softworks (c)

#pragma once

#include "CoreMinimal.h"
#include "ReplicatedComponent.h"
#include "ReplicatedObject.h"
#include "GameFramework/Character.h"
#include "Task4Character.generated.h"

UCLASS()
class REDRUINS_TT_2024_API ATask4Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATask4Character();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable)
	void SpawnReplicatedComponent();

	UFUNCTION(Server, Reliable)
	void RoS_SpawnReplicatedComponent();
	
protected:

	UPROPERTY(Replicated, BlueprintReadWrite, EditAnywhere)
	UReplicatedObject* ReplicatedObject;

	UPROPERTY(Replicated, BlueprintReadOnly, VisibleAnywhere)
	UReplicatedComponent* ReplicatedComponent;
};
