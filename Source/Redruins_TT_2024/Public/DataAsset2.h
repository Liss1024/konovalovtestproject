// RedRuins Softworks (c)

#pragma once

#include "CoreMinimal.h"
#include "DataAsset1.h"
#include "Engine/DataAsset.h"
#include "DataAsset2.generated.h"

UENUM(BlueprintType)
enum EDA2Enum
{
	value_true		UMETA(DisplayName = "True"),
	value_false		UMETA(DisplayName = "False"),
	value_n			UMETA(DisplayName = "N"),
};

UCLASS(BlueprintType)
class REDRUINS_TT_2024_API UDataAsset2 : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TEnumAsByte<EDA2Enum> Enum;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (EditCondition = "Enum == EDA2Enum::value_true", EditConditionHides))
	UDataAsset1* DataAsset1;
};
