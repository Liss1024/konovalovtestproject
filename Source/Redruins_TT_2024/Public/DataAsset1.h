// RedRuins Softworks (c)

#pragma once

#include "CoreMinimal.h"
#include "DataAssetStructs.h"
#include "Engine/DataAsset.h"
#include "DataAsset1.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class REDRUINS_TT_2024_API UDataAsset1 : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	FDataAsset1Struct DA1Struct;
};
