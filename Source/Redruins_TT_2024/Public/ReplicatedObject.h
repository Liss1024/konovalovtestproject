// RedRuins Softworks (c)

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ReplicatedObject.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class REDRUINS_TT_2024_API UReplicatedObject : public UObject
{
	GENERATED_BODY()

public:

	UReplicatedObject();

	virtual bool IsSupportedForNetworking() const override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:

	UPROPERTY(Replicated, BlueprintReadWrite, EditAnywhere)
	bool Value1;

	UPROPERTY(Replicated, BlueprintReadWrite, EditAnywhere)
	float Value2;

	UPROPERTY(Replicated, BlueprintReadWrite, EditAnywhere)
	int32 Value3;
};
