// RedRuins Softworks (c)


#include "Task4Character.h"

#include "Engine/ActorChannel.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ATask4Character::ATask4Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	ReplicatedComponent = nullptr;
	ReplicatedObject = nullptr;
	
}

// Called when the game starts or when spawned
void ATask4Character::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATask4Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATask4Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

bool ATask4Character::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{

	if (IsValid(ReplicatedObject))
	{
		Channel->ReplicateSubobject(ReplicatedObject, *Bunch, *RepFlags);
	}
	
	return Super::ReplicateSubobjects(Channel, Bunch, RepFlags);
}

void ATask4Character::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATask4Character, ReplicatedObject);
	DOREPLIFETIME(ATask4Character, ReplicatedComponent);
}

void ATask4Character::SpawnReplicatedComponent()
{
	RoS_SpawnReplicatedComponent();
}

void ATask4Character::RoS_SpawnReplicatedComponent_Implementation()
{
	ReplicatedComponent = NewObject<UReplicatedComponent>(this, UReplicatedComponent::StaticClass(), TEXT("ReplicatedComponent"));
	ReplicatedComponent->RegisterComponent();
	ReplicatedComponent->SetIsReplicated(true);
}

