// RedRuins Softworks (c)


#include "ReplicatedObject.h"

#include "Net/UnrealNetwork.h"

UReplicatedObject::UReplicatedObject()
{
	Value1 = false;
	Value2 = 0.0f;
	Value3 = 0;
}

bool UReplicatedObject::IsSupportedForNetworking() const
{
	return true;
}

void UReplicatedObject::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UReplicatedObject, Value1);
	DOREPLIFETIME(UReplicatedObject, Value2);
	DOREPLIFETIME(UReplicatedObject, Value3);

	if (UBlueprintGeneratedClass* BPClass = Cast<UBlueprintGeneratedClass>(GetClass()))
	{
		BPClass->GetLifetimeBlueprintReplicationList(OutLifetimeProps);
	}
}

